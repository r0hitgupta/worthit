import React, { Component } from "react";
import { Route, Switch, Redirect, Link } from "react-router-dom";
import "./Login.css";
import {login} from '../../api/api';

class Login extends Component {
  state = {
    loading: false
  }
  login = (event) => {
    this.setState({
      loading: true
    });
    event.preventDefault();
    const username= event.target.username.value;
    const password= event.target.password.value;
    if(username && password){
      login(username, password).then(res => {
        if(res.success){
          console.log(res);
          localStorage.setItem('token', res.data.access_token);
          this.props.history.push('/dashboard');
        }
        else{
          window.alert('Invalid username or password');
          this.setState({
            loading: false
          })
        }
      }
      );
    }
  }
  render() {
    return (
      <div
        className="wrapper"
        style={{
          backgroundColor: "#eaeaea",
          display: "flex",
          justifyContent: "center",
          alignItems: "center"
        }}
      >
        <form class="form-signin" onSubmit={this.login}>
          <h2 class="form-signin-heading">Please login</h2>
          <input
            type="text"
            class="form-control"
            name="username"
            placeholder="Username"
            required=""
            autofocus=""
          />
          <input
            type="password"
            class="form-control"
            name="password"
            placeholder="Password"
            required
          />
          <hr />
          {/* <Link to="/dashboard"> */}
            <button class="btn btn-lg btn-primary btn-block" type="submit">
              {this.state.loading ? 'Please Wait' : 'Login'}
            </button>
          {/* </Link> */}
        </form>
      </div>
    );
  }
}

export default Login;
