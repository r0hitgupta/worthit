import React, { Component } from "react";
import { Grid, Row, Col, Table } from "react-bootstrap";

import Card from "../../components/Card/Card.jsx";
import CustomButton from "../../components/CustomButton/CustomButton.jsx";
import FormInputs from "../../components/FormInputs/FormInputs.jsx";
import { getCategoryById } from "../../api/api";

class CategoryForm extends Component {
  state = {
    isLoading: true,
    data: []
  };
  componentDidMount() {
      getCategoryById(this.props.match.params.categoryId).then(data =>
        this.setState({
            isLoading: false,
            data
          })
        );
    
  }
  render() {
    const {data, isLoading} = this.state;
    const { match } = this.props;
    return (
        <Grid fluid>
      {isLoading && <p>Loading...</p>}

          {!isLoading && <Row>
            <Col md={12}>
              <Card
                title="Edit Category"
                ctTableFullWidth
                ctTableResponsive
                content={
                  <div style={{padding: 15}}>
                    <FormInputs
                    ncols = {["col-md-3" , "col-md-4"]}
                    proprieties = {[
                        {
                            label : "Category Name",
                            type : "text",
                            bsClass : "form-control",
                            placeholder : "Category Name",
                            defaultValue : ""
                        },
                        {
                            label : "Image",
                            type : "file",
                            bsClass : "form-control",
                            placeholder : "Image"
                        }
                    ]}
                />
                </div>
                }
              />
            </Col>
          </Row>}
          <CustomButton fill bsStyle="primary">
            SAVE
          </CustomButton>
        </Grid>
    );
  }
}

export default CategoryForm;
