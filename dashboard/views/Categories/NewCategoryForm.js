import React, { Component } from "react";
import { Grid, Row, Col, Table, Alert } from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import CustomButton from "components/CustomButton/CustomButton.jsx";
import FormInputs from "components/FormInputs/FormInputs.jsx";
import { addNewCategory } from "../../api/api";

class NewCategoryForm extends React.Component {
  state = {
    isLoading: false,
    data: [],
    showAlert: false,
    alertMessage: ""
  };

  handleSubmit = event => {
    event.preventDefault();
    this.setState({ showAlert: false });
    let categoryName = event.target.categoryName.value;
    let imageToUpload = event.target.imageToUpload.files[0];
    if (imageToUpload && categoryName) {
      addNewCategory(categoryName, imageToUpload)
        .then(res => {
          console.log(res);
          this.setState({ alertMessage: res.message, showAlert: true });
        })
        .catch(err => {
          console.log(err);
          this.setState({ alertMessage: err, showAlert: true });
        });
    }
  };
  render() {
    const { data, isLoading } = this.state;
    const { match } = this.props;
    return (
      <Grid fluid>
        {this.state.showAlert && (
          <Alert bsStyle="info">{this.state.alertMessage}</Alert>
        )}
        <form onSubmit={this.handleSubmit}>
          <Row>
            <Col md={12}>
              <Card
                title="New Category"
                ctTableFullWidth
                ctTableResponsive
                content={
                  <div style={{ padding: 15 }}>
                    <FormInputs
                      ncols={["col-md-3", "col-md-4"]}
                      proprieties={[
                        {
                          label: "Category Name",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Category Name",
                          defaultValue: "",
                          name: "categoryName",
                          required: true
                        },
                        {
                          label: "Image",
                          type: "file",
                          bsClass: "form-control",
                          placeholder: "Image",
                          name: "imageToUpload",
                          required: true

                        }
                      ]}
                    />
                  </div>
                }
              />
            </Col>
          </Row>
          <CustomButton fill bsStyle="primary" type="submit">
            SAVE
          </CustomButton>
        </form>
      </Grid>
    );
  }
}
export default NewCategoryForm;
