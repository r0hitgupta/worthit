import React, { Component } from "react";
import { Grid, Row, Col, Table } from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import CustomButton from "components/CustomButton/CustomButton.jsx";
import CategoryForm from "views/Categories/NewCategoryForm.js";
import { getAllCategories } from "../../api/api";
import { Route, Link } from "react-router-dom";

class CategoryList extends Component {
  state = {
    isLoading: true,
    data: []
  };
  componentDidMount() {
    getAllCategories().then(data => {
      console.log(data);
      this.setState({
        isLoading: false,
        data
      })
    }
    ).catch(err => console.log(err));
  }
  render() {
    const {data, isLoading} = this.state;
    const { match } = this.props;

    return (
      <Grid fluid>
      {isLoading && <p>Loading...</p>}
        {!isLoading && (<Row>
          <Col md={12}>
            <Card
              title="Manage Categories"
              category=""
              ctTableFullWidth
              ctTableResponsive
              content={
                <Table striped hover>
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Name</th>
                      <th>Icon</th>
                      <th />
                    </tr>
                  </thead>
                  <tbody>
                    {data.map((category, key) => {
                      return (
                        <tr key={key}>
                          <td>{category["id"]}</td>
                          <td>{category["name"]}</td>
                          <td>
                            <img
                              src={category["thumbnail"]}
                              width="50"
                              height="50"
                            />
                          </td>
                          <td><Link to={`${match.url}/${category["id"]}`}>EDIT</Link></td>
                        </tr>
                      );
                    })}
                  </tbody>
                </Table>
              }
            />
          </Col>
        </Row>
        )}
        <Link to={`${match.url}/new`}>
        <CustomButton fill bsStyle="primary">
          ADD NEW CATEGORY
        </CustomButton>
        </Link>
      </Grid>
    );
  }
}

const Categories = ({ match }) => (
  <div className="content">
    <Route path={`${match.url}/:categoryId`} component={CategoryForm} />
    <Route exact path={`${match.url}/new`} component={CategoryForm} />
    <Route exact path={`${match.url}/`} component={CategoryList} />
  </div>
);

export default Categories;
