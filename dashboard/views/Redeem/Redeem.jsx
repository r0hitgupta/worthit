import React, { Component } from "react";
import { Grid, Row, Col, Table, Alert } from "react-bootstrap";

import Card from "../../components/Card/Card.jsx";
import CustomButton from "../../components/CustomButton/CustomButton.jsx";
import FormInputs from "../../components/FormInputs/FormInputs.jsx";
import { redeemCoupon } from "../../api/api";

class Redeem extends React.Component {
  state = {
    isLoading: false,
    data: [],
    showAlert: false,
    alertMessage: ""
  };

  handleSubmit = event => {
    event.preventDefault();
    this.setState({ showAlert: false });
    let code = event.target.code.value;
    if (code) {
      redeemCoupon(code)
        .then(res => {
          this.setState({ alertMessage: res.message, showAlert: true });
        })
        .catch(err => {
          console.log(err);
          this.setState({ alertMessage: "Error occured", showAlert: true });
        });
    }
  };
  render() {
    const { data, isLoading } = this.state;
    const { match } = this.props;
    return (
      <div className="content">

      <Grid fluid>
        {this.state.showAlert && (
          <Alert bsStyle="info">{this.state.alertMessage}</Alert>
        )}
        <form onSubmit={this.handleSubmit}>
          <Row>
            <Col md={12}>
              <Card
                title="Redeem Coupon"
                ctTableFullWidth
                ctTableResponsive
                content={
                  <div style={{ padding: 15 }}>
                    <FormInputs
                      ncols={["col-md-12"]}
                      proprieties={[
                        {
                          label: "CODE",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Code",
                          defaultValue: "",
                          name: "code",
                          required: true
                        },
                      ]}
                    />
                  </div>
                }
              />
            </Col>
          </Row>
          <CustomButton fill bsStyle="primary" type="submit">
            REDEEM
          </CustomButton>
        </form>
      </Grid>
      </div>
    );
  }
}
export default Redeem;
