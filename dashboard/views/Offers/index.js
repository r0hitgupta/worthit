import React, { Component } from "react";
import { Grid, Row, Col, Table, Alert } from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import CustomButton from "components/CustomButton/CustomButton.jsx";
import FormInputs from "components/FormInputs/FormInputs.jsx";
import { addNewOffer } from "../../api/api";
import queryString from 'query-string';

class Offers extends React.Component {
  state = {
    isLoading: false,
    data: [],
    showAlert: false,
    alertMessage: ""
  };
  
  handleSubmit = event => {
    event.preventDefault();
    const qs = queryString.parse(this.props.location.search);
    this.setState({ showAlert: false });
    let title = event.target.title.value;
    let description = event.target.description.value;
    if (title && description && qs["storeId"]) {
      addNewOffer(qs["storeId"], title, description)
        .then(res => {
          console.log(res);
          this.setState({ alertMessage: res.message, showAlert: true });
        })
        .catch(err => {
          console.log(err);
          this.setState({ alertMessage: err, showAlert: true });
        });
    }
  };
  render() {
    const { data, isLoading } = this.state;
    const { match } = this.props;
    return (
      <Grid fluid>
        {this.state.showAlert && (
          <Alert bsStyle="info">{this.state.alertMessage}</Alert>
        )}
        <form onSubmit={this.handleSubmit}>
        {/* <form> */}
          <Row>
            <Col md={12}>
              <Card
                title="Offers"
                ctTableFullWidth
                ctTableResponsive
                content={
                  <div style={{ padding: 15 }}>
                    <FormInputs
                      ncols={["col-md-4", "col-md-12"]}
                      proprieties={[
                        {
                          label: "Title",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Title",
                          defaultValue: "",
                          name: "title",
                          required: true
                        },
                        {
                          label: "Description",
                          type: "textarea",
                          bsClass: "form-control",
                          placeholder: "Description",
                          defaultValue: "",
                          name: "description",
                          required: true
                        },
                      ]}
                    />
                  </div>
                }
              />
            </Col>
          </Row>
          <CustomButton fill bsStyle="primary" type="submit">
            SAVE
          </CustomButton>
        </form>
      </Grid>
    );
  }
}
export default Offers;
