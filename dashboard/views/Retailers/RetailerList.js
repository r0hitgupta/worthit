import React, { Component } from "react";
import { Grid, Row, Col, Table } from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import CustomButton from "components/CustomButton/CustomButton.jsx";
import { getAllRetailers } from "../../api/api";
import { Route, Link } from "react-router-dom";

class RetailerList extends Component {
  state = {
    isLoading: true,
    data: []
  };
  componentDidMount() {
    getAllRetailers()
      .then(res =>
        this.setState({
          isLoading: false,
          data: res.data
        })
      )
      .catch(err => console.log(err));
  }
  render() {
    const data = this.state.data;
    const { match } = this.props;
    console.log(data);

    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="Manage Retailers"
                category=""
                ctTableFullWidth
                ctTableResponsive
                content={
                  <Table striped hover>
                    <thead>
                      <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th>Number of Ratings</th>
                        <th />
                      </tr>
                    </thead>
                    <tbody>
                      {data.map((retailer, key) => {
                        return (
                          <tr key={key}>
                            <td>{retailer["id"]}</td>
                            <td>{retailer["name"]}</td>
                            <td>{retailer["address"]}</td>
                            <td>{retailer["total_ratings"]}</td>
                            <td>
                              <Link
                                to={{
                                  pathname: `${match.url}/${retailer["id"]}`,
                                  state: { retailer }
                                }}
                              >
                                EDIT
                              </Link>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                }
              />
            </Col>
          </Row>
          <Link to={`${match.url}/new`}>
            <CustomButton fill bsStyle="primary">
              ADD NEW RETAILER
            </CustomButton>
          </Link>
        </Grid>
      </div>
    );
  }
}

export default RetailerList;
