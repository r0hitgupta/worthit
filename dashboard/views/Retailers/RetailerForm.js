import React, { Component } from "react";
import { Grid, Row, Col, Table, Alert } from "react-bootstrap";
import { FormGroup, ControlLabel, FormControl } from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import CustomButton from "components/CustomButton/CustomButton.jsx";
import FormInputs from "components/FormInputs/FormInputs.jsx";
import { getAllCategories, addNewRetailer } from "../../api/api";
import { Route, Link } from "react-router-dom";

function FieldGroup({ label, ...props }) {
  return (
    <FormGroup>
      <ControlLabel>{label}</ControlLabel>
      <FormControl {...props}>{props.children}</FormControl>
    </FormGroup>
  );
}

class RetailerForm extends Component {
  state = {
    isLoading: true,
    categories: [],
    retailer: {}
  };

  componentDidMount() {
    console.log(this.props.location);
    if (
      this.props.location.state !== undefined &&
      this.props.location.state.hasOwnProperty("retailer")
    ) {
      this.setState({
        isNew: false,
        retailer: this.props.location.state.retailer
      });
    }
    getAllCategories().then(categories =>
      this.setState({
        isLoading: false,
        categories
      })
    );
  }
  handleChange = (event, field) => {
    if(field === "imageToUpload"){
      console.log(event.target.files[0]);
      this.setState({
        retailer: {
          ...this.state.retailer,
          [field]: event.target.files[0]
        }
      });
    }
    else {

      this.setState({
        retailer: {
          ...this.state.retailer,
          [field]: event.target.value
        }
      });
    }
  };

  handleSubmit = event => {
    console.log("aaya");
    event.preventDefault();
    this.setState({ showAlert: false });
    console.log(this.state.retailer);
    addNewRetailer(this.state.retailer)
      .then(res => {
        console.log(res);
        this.setState({ alertMessage: res.message, showAlert: true });
      })
      .catch(err => {
        console.log(err);
        this.setState({ alertMessage: err, showAlert: true });
      });
  };

  render() {
    const { data, isLoading, retailer } = this.state;
    const { match } = this.props;
    return (
      <Grid fluid>
        {this.state.showAlert && (
          <Alert bsStyle="info">{this.state.alertMessage}</Alert>
        )}
        <form onSubmit={this.handleSubmit}>
          <Row>
            <Col md={12}>
              <Card
                title="Edit Retailer"
                ctTableFullWidth
                ctTableResponsive
                content={
                  <div style={{ padding: 15 }}>
                    <Row>
                      <Col md={6}>
                        <FieldGroup
                          label="Retailer Name"
                          type="text"
                          required
                          bsClass="form-control"
                          name="name"
                          placeholder="Retailer Name"
                          onChange={e => this.handleChange(e, "name")}
                          value={retailer.name || ""}
                        />
                      </Col>

                      <Col md={4}>
                        <FieldGroup
                          label="Phone Number"
                          type="number"
                          required
                          name="phone"
                          bsClass="form-control"
                          placeholder="Phone Number"
                          onChange={e => this.handleChange(e, "phone")}
                          value={retailer.phone || ""}
                        />
                      </Col>
                      <Col md={6}>
                        <FieldGroup
                          label="Manager Name"
                          type="text"
                          required
                          bsClass="form-control"
                          name="managerName"
                          placeholder="Manager Name"
                          onChange={e => this.handleChange(e, "name")}
                          value={retailer.name || ""}
                        />
                      </Col>

                      <Col md={4}>
                        <FieldGroup
                          label="Manager Phone Number"
                          type="number"
                          required
                          name="managerPhone"
                          bsClass="form-control"
                          placeholder="Manager Phone Number"
                          onChange={e => this.handleChange(e, "phone")}
                          value={retailer.phone || ""}
                        />
                      </Col>
                      
                    </Row>

                    <Row>
                      <Col md={12}>
                        <FieldGroup
                          label="Address"
                          type="text"
                          required
                          bsClass="form-control"
                          name="address"
                          placeholder="Address"
                          onChange={e => this.handleChange(e, "address")}
                          value={retailer.address || ""}
                        />
                      </Col>
                    </Row>

                    <Row>
                      <Col md={6}>
                        <FieldGroup
                          label="Category"
                          bsClass="form-control"
                          placeholder="Select Category"
                          componentClass="select"
                          name="category"
                          required
                          onChange={e => this.handleChange(e, "category")}
                          value={retailer.category || ""}
                        >
                          <option>Select a Category</option>

                          {this.state.categories.map(category => (
                            <option key={category.id} value={category.id}>
                              {category.name}
                            </option>
                          ))}
                        </FieldGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md={4}>
                        <FieldGroup
                          label="Username"
                          type="text"
                          required
                          bsClass="form-control"
                          placeholder="Username"
                          name="username"
                          onChange={e => this.handleChange(e, "username")}
                          value={retailer.username || ""}
                        />
                      </Col>

                      <Col md={4}>
                        <FieldGroup
                          label="Password"
                          type="text"
                          required
                          bsClass="form-control"
                          name="password"
                          placeholder="Password"
                          onChange={e => this.handleChange(e, "password")}
                          value={retailer.password || ""}
                        />
                      </Col>
                    </Row>
                    <Row>
                      <Col md={4}>
                        <FieldGroup
                          label="Images"
                          type="file"
                          bsClass="form-control"
                          name="imageToUpload"
                          placeholder="Images"
                          onChange={e => this.handleChange(e, "imageToUpload")}
                        />
                      </Col>
                    </Row>
                  </div>
                }
              />
            </Col>
          </Row>
          <CustomButton fill bsStyle="primary" type="submit">
            SAVE
          </CustomButton>
          <span style={{paddingLeft: 10}} />
          <Link to={"offers/add?storeId="+retailer.id}><CustomButton fill bsStyle="primary">
            OFFERS
          </CustomButton></Link>
        </form>
        
      </Grid>
    );
  }
}

export default RetailerForm;
