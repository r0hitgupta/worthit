import React, { Component } from "react";
import { Route, Link } from "react-router-dom";
import RetailerList from "./RetailerList";
import RetailerForm from "./RetailerForm";
import Offers from "../Offers";

const Retailers = ({ match }) => (
    <div className="content">
      <Route exact path={`${match.url}/offers/add`} component={Offers} />
      <Route exact path={`${match.url}/`} component={RetailerList} />
      <Route exact path={`${match.url}/:retailerId`} component={RetailerForm} />
    </div>
  );
  
  export default Retailers;