import Dashboard from "../layouts/Dashboard/Dashboard.jsx";
import Login from "../layouts/Login/Login.jsx";
import React, { PureComponent } from "react";
import { Route, Redirect } from "react-router-dom";

const PrivateRoute = () => (
  <Route
    render={props =>
      localStorage.getItem("token") !== null ? (
        <Dashboard {...props} />
      ) : (
        <Redirect to="/login" />
      )
    }
  />
);

var indexRoutes = [
  { path: "/login", name: "Login", component: Login },
  { path: "/", name: "Home", component: PrivateRoute }
];

export default indexRoutes;
